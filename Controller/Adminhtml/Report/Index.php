<?php

namespace Magenest\Slider\Controller\Adminhtml\Report;

class Index extends \Magenest\Slider\Controller\Adminhtml\Report
{
    public function execute()
    {
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Magenest_Slider::report_click');
        return $resultPage;
    }
}
