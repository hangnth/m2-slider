<?php

namespace Magenest\Slider\Controller\Adminhtml;

use Magento\Backend\App\Action;

abstract class Report extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Magenest_Slider::report';

    protected $_resultPageFactory;

    /**
     * Report constructor.
     *
     * @param \Magento\Backend\App\Action\Context        $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
}
