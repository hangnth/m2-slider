<?php

namespace Magenest\Slider\Controller\Adminhtml\Import;

use Magenest\Slider\Controller\Adminhtml\Slider;
use Magento\Backend\App\Action;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magenest\Slider\Model\Slider as SliderModel;
use Magenest\Slider\Model\Item as ItemModel;
use Magento\Framework\Serialize\SerializerInterface;

/**
 * Class Save
 *
 * @package Magenest\Slider\Controller\Adminhtml\Import
 */
class Save extends Slider
{
    const ADMIN_RESOURCE = 'Magenest_Slider::import';
    const NAME = 'name';
    const STATUS = 'status';
    const TYPE = 'type';
    const DATA_SOURCE_SLIDER = 'data_source_slider';
    const PARENT_ID = 'parent_id';
    const DATA_SOURCE_ITEM = 'data_source_item';
    const ORDER_NUMBER = 'order_number';

    protected $uploaderFactory;

    protected $varDirectory;

    protected $filesystem;

    protected $csvProcessor;

    protected $sliderFactory;

    protected $itemFactory;

    protected $sliderResourceModel;

    protected $itemResourceModel;

    protected $_slider;

    protected $_item;

    protected $_dir;

    protected $_path;

    protected $serializer;

    public function __construct(
        Action\Context $context,
        \Magento\Widget\Model\Widget\InstanceFactory $widgetFactory,
        \Magento\MediaStorage\Model\File\UploaderFactory $uploaderFactory,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\File\Csv $csvProcessor,
        \Magenest\Slider\Model\SliderFactory $sliderFactory,
        \Magenest\Slider\Model\ItemFactory $itemFactory,
        \Magenest\Slider\Model\ResourceModel\Slider $sliderResourceModel,
        \Magenest\Slider\Model\ResourceModel\Item $itemResourceModel,
        \Magento\Framework\Filesystem\DirectoryList $dir,
        \Magento\Framework\Component\ComponentRegistrarInterface $path,
        \Psr\Log\LoggerInterface $logger,
        SerializerInterface $serializer,
        SliderModel $slider,
        ItemModel $item
    ) {
        $this->uploaderFactory     = $uploaderFactory;
        $this->filesystem          = $filesystem;
        $this->csvProcessor        = $csvProcessor;
        $this->varDirectory        = $this->filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        $this->sliderFactory       = $sliderFactory;
        $this->itemFactory         = $itemFactory;
        $this->sliderResourceModel = $sliderResourceModel;
        $this->itemResourceModel   = $itemResourceModel;
        $this->_slider             = $slider;
        $this->_dir                = $dir;
        $this->_path               = $path;
        $this->_item               = $item;
        $this->serializer          = $serializer;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     * @throws \Exception
     */
    public function execute()
    {
        try {
            $rootPath = $this->_path->getPath('module', 'Magenest_Slider');
            $rootPub  = $this->_dir->getPath('media');
            if (!file_exists($rootPub . '/catalog/tmp/category')) {
                mkdir($rootPub . '/catalog/tmp/category', 0777, true);
            }
            $filePath         = $rootPath . '/view/adminhtml/web/images';
            $copyFileFullPath = $rootPub . '/catalog/tmp/category';

            $this->copy_directory($filePath, $copyFileFullPath);
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setPath('pin/code/index', ['_current' => false]);
            //upload file
            $uploader     = $this->uploaderFactory->create(['fileId' => 'import_file']);
            $uploadDir    = $this->varDirectory->getAbsolutePath('importexport/');
            $result       = $uploader->save($uploadDir);
            $uploadedFile = $result['path'] . $result['file'];
            if ($result['type'] == "text/xml") {
                try {
                    $xml   = simplexml_load_file($uploadedFile);
                    $table = $xml->Worksheet->Table;
                    $head  = $this->getHead($xml->Worksheet->Table);
                    if (count(
                        array_diff(
                            [
                            self::NAME,
                            self::STATUS,
                            self::TYPE,
                            self::DATA_SOURCE_SLIDER,
                            self::PARENT_ID,
                            self::DATA_SOURCE_ITEM,
                            self::ORDER_NUMBER,
                            ], $head
                        )
                    )
                    ) {
                        $this->messageManager->addErrorMessage("The file structure is not properly formatted");
                        return $resultRedirect->setPath('*/*/index', ['_current' => true]);
                    }
                    $data = $this->getData($table, $head);
                    $i    = 0;

                    if (count($data) == 0) {
                        $this->messageManager->addErrorMessage('The file is empty');
                        return $resultRedirect->setPath('*/*/index', ['_current' => true]);
                    }

                    foreach ($data as $record) {
                        $slider = $this->saveSlider($record);
                        if ($slider) {
                            $i++;
                        }
                    }

                    if ($i == 0) {
                        return $resultRedirect->setPath('*/*/index', ['_current' => true]);
                    }

                    $this->messageManager->addSuccessMessage(__('A total of %1 record(s) have been saved', $i));
                } catch (\Exception $exception) {
                    $this->messageManager->addErrorMessage($exception->getMessage());
                    return $resultRedirect->setPath('*/*/index', ['_current' => true]);
                }
            } else {
                try {
                    //get csv adapter
                    $csvAdapter = \Magento\ImportExport\Model\Import\Adapter::findAdapterFor(
                        $uploadedFile,
                        $this->filesystem->getDirectoryWrite(DirectoryList::ROOT),
                        ','
                    );

                    $i = 0;
                    $n = 0;
                    foreach ($csvAdapter as $rowNum => $record) {
                        if (count(
                            array_diff(
                                [
                                self::NAME,
                                self::STATUS,
                                self::TYPE,
                                self::DATA_SOURCE_SLIDER,
                                self::PARENT_ID,
                                self::DATA_SOURCE_ITEM,
                                self::ORDER_NUMBER,
                                ], $csvAdapter->getColNames()
                            )
                        )
                        ) {
                            $this->messageManager->addErrorMessage("The file structure is not properly formatted");
                            return $resultRedirect->setPath('*/*/index', ['_current' => true]);
                        }
                        $slider = $this->saveSlider($record);
                        if ($slider) {
                            $i++;
                        }
                        $n++;
                    }

                    if ($n == 0) {
                        $this->messageManager->addErrorMessage('The file is empty');
                        return $resultRedirect->setPath('*/*/index', ['_current' => true]);
                    }

                    if ($i) {
                        $this->messageManager->addSuccessMessage(__('A total of %1 record(s) have been saved', $i));
                    } else {

                    }
                } catch (\Exception $e) {
                    $this->messageManager->addErrorMessage($e->getMessage());
                    return $resultRedirect->setPath('*/*/index', ['_current' => true]);
                }
            }
        } catch (\Exception $exception) {
            $this->messageManager->addErrorMessage($exception->getMessage());
            return $resultRedirect->setPath('*/*/index', ['_current' => true]);
        }
        $this->getSliderCollection()->save();

        $this->getItemCollection()->save();

        return $resultRedirect->setPath('*/*/index', ['_current' => true]);
    }

    protected function getData($table, $head)
    {
        $result = [];
        for ($i = 0; $i < count($table->Slider); $i++) {
            $data = $table->Slider[$i]->Cell;
            $temp = [];
            for ($index = 0; $index < count($data); $index++) {
                $temp[$head[$index]] = (string)$data[$index]->Data;
            }

            array_push($result, $temp);
        }

        return $result;
    }

    protected function getHead($table)
    {
        $head   = $table->Head->Cell;
        $result = [];
        foreach ($head as $column) {
            $x = (string)$column->Data;
            array_push($result, $x);
        }

        return $result;
    }

    public function getSliderCollection()
    {
        return $this->_slider->getCollection();
    }

    public function getItemCollection()
    {
        return $this->_item->getCollection();
    }

    protected function saveSlider($data)
    {
        $dataSlider = [
            'name'        => $data[self::NAME],
            'status'      => $data[self::STATUS],
            'type'        => $data[self::TYPE],
            'data_source' => $data[self::DATA_SOURCE_SLIDER],
            'parent_id'   => $data[self::PARENT_ID],
        ];

        if ($data) {
            $model = $this->sliderFactory->create();

            if (!empty($data['slider_id'])) {
                $model->load($data['slider_id']);
                if ($data['slider_id'] != $model->getId()) {
                    throw new \Exception(__('Wrong slider.'));
                }
            }
            $model->setData($dataSlider);
            $this->_session->setPageData($model->getData());
            try {
                $model->save();

                $id        = $this->getRequest()->getParam('id');
                $itemModel = $this->itemFactory->create();

                $this->itemResourceModel->load($itemModel, $id);
                $this->sliderResourceModel->load($model, $id);

                $source = $this->serializer->unserialize($data[self::DATA_SOURCE_ITEM]);
                if (isset($source['customImageUrl'], $source['customImageUrl']['url'])) {
                    $source['customImageUrl']['url'] = $this->_url->getBaseUrl() . 'pub/media/catalog/tmp/category/' . $source['customImageUrl']['url'];
                    $data[self::DATA_SOURCE_ITEM] = $this->serializer->serialize($source);
                }
                $itemModel->addData(
                    [
                    'slider_id'    => $model->getId(),
                    'data_source'  => $data[self::DATA_SOURCE_ITEM],
                    'order_number' => $data[self::ORDER_NUMBER],
                    ]
                );

                $this->itemResourceModel->save($itemModel);

                $this->_session->setPageData(false);
                return true;
            } catch (\Exception $e) {
                $this->messageManager->addWarningMessage($e->getMessage());
            }
        }
    }

    protected function copy_directory($src, $dst)
    {
        $dir = opendir($src);
        if (!file_exists($dst)){
            mkdir($dst);
        }
        while (false !== ($file = readdir($dir))) {
            if (($file != '.') && ($file != '..')) {
                if (is_dir($src . '/' . $file)) {
                    recurse_copy($src . '/' . $file, $dst . '/' . $file);
                } else {
                    copy($src . '/' . $file, $dst . '/' . $file);
                }
            }
        }
        closedir($dir);
    }
}
