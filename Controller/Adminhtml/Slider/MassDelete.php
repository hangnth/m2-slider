<?php

namespace Magenest\Slider\Controller\Adminhtml\Slider;

use Exception;
use Magenest\Slider\Model\ResourceModel\Item\CollectionFactory;
use Magenest\Slider\Model\ResourceModel\Slider;
use Magento\Backend\App\Action;
use Magento\Framework\App\Cache\TypeListInterface as CacheTypeListInterface;
use Magento\Ui\Component\MassAction\Filter;

class MassDelete extends \Magenest\Slider\Controller\Adminhtml\Slider
{
    protected $sliderResourceModel;

    protected $itemCollection;

    protected $sliderCollectionFactory;

    protected $_filter;

    protected $cache;

    /**
     * MassDelete constructor.
     *
     * @param \Magento\Backend\App\Action\Context                           $context
     * @param \Magenest\Slider\Model\ResourceModel\Slider                   $sliderResourceModel
     * @param \Magenest\Slider\Model\ResourceModel\Slider\CollectionFactory $sliderCollectionFactory
     * @param \Magento\Framework\App\Cache\TypeListInterface                $cache
     * @param \Magento\Ui\Component\MassAction\Filter                       $filter
     */
    public function __construct(
        Action\Context $context,
        Slider $sliderResourceModel,
        \Magenest\Slider\Model\ResourceModel\Slider\CollectionFactory $sliderCollectionFactory,
        CacheTypeListInterface $cache,
        Filter $filter
    ) {
        parent::__construct($context);
        $this->sliderResourceModel     = $sliderResourceModel;
        $this->sliderCollectionFactory = $sliderCollectionFactory;
        $this->_filter                 = $filter;
        $this->cache                   = $cache;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $resultRedirect   = $this->resultRedirectFactory->create();
        $collection       = $this->_filter->getCollection($this->sliderCollectionFactory->create());
        $numSliderDeleted = 0;
        foreach ($collection as $slider) {
            try {
                $this->sliderResourceModel->delete($slider);
                $numSliderDeleted++;
            } catch (Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('*/*/');
            }
        }
        $this->cache->invalidate(['layout', 'block_html', 'full_page']);
        $this->messageManager->addSuccessMessage(__('You have successfully deleted %1 item(s).', $numSliderDeleted));
        return $resultRedirect->setPath('*/*/');
    }
}
