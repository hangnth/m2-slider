<?php

namespace Magenest\Slider\Controller\Adminhtml\Slider;

use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\Cache\Manager as CacheManager;
use Magento\Framework\App\Cache\TypeListInterface as CacheTypeListInterface;

class Save extends \Magenest\Slider\Controller\Adminhtml\Slider
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Magenest\Slider\Model\SliderFactory
     */
    protected $sliderFactory;

    /**
     * @var \Magenest\Slider\Model\ResourceModel\Item\Collection
     */
    protected $itemCollection;
    /**
     * @var \Magenest\Slider\Model\ItemFactory
     */
    protected $itemFactory;

    protected $_storeManager;

    /**
     * @var CacheTypeListInterface
     */
    protected $cache;

    /**
     * @var CacheManager
     */
    protected $cacheManager;
    /**
     * @var \Magenest\Slider\Model\ResourceModel\Item
     */
    protected $itemResourceModel;
    /**
     * @var \Magenest\Slider\Model\ResourceModel\Slider
     */
    protected $sliderResourceModel;

    public function __construct(
        Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry,
        \Magenest\Slider\Model\SliderFactory $sliderFactory,
        \Magenest\Slider\Model\ItemFactory $itemFactory,
        \Magenest\Slider\Model\ResourceModel\Slider $sliderResourceModel,
        \Magenest\Slider\Model\ResourceModel\Item $itemResourceModel,
        \Magenest\Slider\Model\ResourceModel\Item\CollectionFactory $itemCollection,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        CacheTypeListInterface $cache,
        CacheManager $cacheManager
    ) {
        $this->resultPageFactory   = $resultPageFactory;
        $this->_storeManager       = $storeManager;
        $this->_coreRegistry       = $registry;
        $this->sliderFactory       = $sliderFactory;
        $this->itemFactory         = $itemFactory;
        $this->cache               = $cache;
        $this->cacheManager        = $cacheManager;
        $this->sliderResourceModel = $sliderResourceModel;
        $this->itemResourceModel   = $itemResourceModel;
        $this->itemCollection      = $itemCollection;
        parent::__construct($context);
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $isPost = $this->getRequest()->getPost();

        if ($isPost) {
            $active        = [
                'active_from' => $this->getRequest()->getParam('active_from'),
                'active_to'   => $this->getRequest()->getParam('active_to'),
            ];
            $sliderId      = $this->getRequest()->getParam('slider_id');
            $sliderConfig  = $this->getRequest()->getParam('sliderConfig');
            $childSliderId = $this->getRequest()->getParam('childSliderId');
            $slider        = $this->getRequest()->getParam('slider');
            $childSlider   = $this->getRequest()->getParam('childSlider');

            try {
                $sliderId = $this->saveSliderSettings($sliderId, json_decode($sliderConfig, true), json_decode($slider, true), $active, 0);
                $this->saveItemSettings($sliderId, json_decode($slider, true)['items']);

                if (json_decode($sliderConfig, true)['type'] == '2') {
                    $childSliderId = $this->saveSliderSettings($childSliderId, json_decode($sliderConfig, true), json_decode($childSlider, true), null, $sliderId);
                    $this->saveItemSettings($childSliderId, json_decode($childSlider, true)['items']);
                }

                // Display invalidate cache
                $this->cache->invalidate(['layout', 'block_html', 'full_page']);
                // Display success message
                $this->messageManager->addSuccessMessage(__('The slider has been saved.'));

                // Check if 'Save and Continue'
                if ($this->getRequest()->getParam('back') == 'edit') {
                    $this->_redirect('*/*/edit', ['id' => $sliderId, '_current' => true]);
                    return;
                }

                // Go to grid page
                return $this->_redirect('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            }

            $formDataRequest = $this->getRequest()->getParams();
            $this->_getSession()->setFormData($formDataRequest);
            return $this->_redirect('*/*/edit', ['id' => $sliderId]);
        }
        return $this->_redirect('*/*');
    }

    public function saveSliderSettings($sliderId, $sliderConfig, $sliderForm, $active, $parent_id)
    {
        $sliderModel = $this->sliderFactory->create();
        if ($sliderId) {
            $this->sliderResourceModel->load($sliderModel, $sliderId);
            $sliderData['slider_id'] = $sliderId;
            if (!$sliderModel->getId()) {
                $this->messageManager->addErrorMessage(__('This post no longer exists.'));
                /**
                 * \Magento\Backend\Model\View\Result\Redirect $resultRedirect
                 */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }

        $dataNotSave = [
            'items',
            'currentItem',
        ];
        $dataSource  = [];
        foreach ($sliderForm as $key => $value) {
            if (!in_array($key, $dataNotSave)) {
                $dataSource[$key] = $sliderForm[$key];
            }
        }
        $sliderData['name']        = $sliderConfig['sliderName'];
        $sliderData['status']      = $sliderConfig['status'];
        $sliderData['type']        = $sliderConfig['type'];
        $sliderData['data_source'] = json_encode($dataSource);
        $sliderData['parent_id']   = $parent_id;

        $sliderModel->setData($sliderData);

        // Save slider
        $this->sliderResourceModel->save($sliderModel);
        return $sliderModel->getId();
    }

    public function saveItemSettings($sliderId, $items)
    {
        $itemModel       = $this->itemFactory->create();
        $itemsCollection = $this->itemCollection->create()->addFieldToFilter('slider_id', $sliderId);
        // remove item
        foreach ($itemsCollection as $itemCollection) {
            $item_id   = $itemCollection['item_id'];
            $is_delete = true;
            foreach ($items as $item) {
                if ($item['id'] == $item_id) {
                    $is_delete = false;
                    break;
                }
            }
            if ($is_delete) {
                $this->itemResourceModel->load($itemModel, $item_id);
                $this->itemResourceModel->delete($itemModel);
            }
        }

        // update or save new item
        foreach ($items as $item) {
            $itemModel = $this->itemFactory->create();
            $itemData  = [];
            if ($item['id'] && $item['id'] != 0) {
                $this->itemResourceModel->load($itemModel, $item['id']);
                if (!$itemModel->getId()) {
                    $this->messageManager->addErrorMessage(__('This item no longer exists.'));
                    /**
                     * \Magento\Backend\Model\View\Result\Redirect $resultRedirect
                     */
                    $resultRedirect = $this->resultRedirectFactory->create();
                    return $resultRedirect->setPath('*/*/');
                }
                $itemData['item_id'] = $item['id'];
            }
            $itemData['slider_id']    = $sliderId;
            $itemData['order_number'] = $item['orderId'];
            $dataNotSave              = [
                'contentBgColorRgb',
                'maxWidthContent',
                'marginLeftContent',
                'marginRightContent',
                'floatContent',
                'imageUploadScope',
                'imageUploaderMageInit',
                'chooseCategoryScope',
                'chooseCategoryInit',
            ];
            $dataSource               = [];
            foreach ($item as $key => $value) {
                if (!in_array($key, $dataNotSave)) {
                    $dataSource[$key] = $item[$key];
                }
            }

            $dataBlockNotSave = [
                'maxWidthBlock',
                'marginLeftBlock',
                'marginRightBlock',
                'floatContentBlock',
            ];

            foreach ($item['blocks'] as $blockKey => $block) {
                foreach ($block as $key => $value) {
                    if (in_array($key, $dataBlockNotSave)) {
                        unset($dataSource['blocks'][$blockKey][$key]);
                    }
                }
            }

            $itemData['data_source'] = json_encode($dataSource);
            $itemModel->setData($itemData);
            $this->itemResourceModel->save($itemModel);
        }
    }
}
