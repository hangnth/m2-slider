<?php

namespace Magenest\Slider\Controller\Adminhtml\Slider;

use Magenest\Slider\Controller\Adminhtml\Slider;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends Slider
{
    protected $resultPageFactory = false;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend((__('Banners/Sliders')));
        $resultPage->setActiveMenu('Magenest_Slider::manage');
        return $resultPage;
    }
}
