<?php

namespace Magenest\Slider\Block\Adminhtml;

class Report extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * Constructor.
     */
    protected function _construct()
    {
        $this->_controller = 'adminhtml_report';
        $this->_blockGroup = 'Magenest_Slider';
        $this->_headerText = __('Reports');
        parent::_construct();
        $this->removeButton('add');
    }
}
