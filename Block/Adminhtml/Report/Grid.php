<?php

namespace Magenest\Slider\Block\Adminhtml\Report;
use Magenest\Slider\Model\ResourceModel\Report\Collection as ReportCollection;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{

    protected $_reportCollectionFactory;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magenest\Slider\Model\ResourceModel\Report\CollectionFactory $reportCollectionFactory,
        array $data = []
    ) {
        $this->_reportCollectionFactory = $reportCollectionFactory;
        parent::__construct($context, $backendHelper, $data);
    }

    protected function _construct()
    {
        parent::_construct();
        $this->setId('reportGrid');
        $this->setDefaultSort('report_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }
    protected function _prepareCollection()
    {
        $collection = $this->_reportCollectionFactory->create();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn(
            'report_id',
            [
                'header' => __('ID'),
                'type' => 'number',
                'index' => 'report_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id',
            ]
        );
        $this->addColumn(
            'name',
            [
                'header' => __('Slider Name'),
                'type' => 'varchar',
                'index' => 'name',
            ]
        );
        $this->addColumn(
            'clicks',
            [
                'header' => __('Clicks'),
                'align' => 'left',
                'index' => 'clicks',
                'type' => 'number',
            ]
        );
        $this->addColumn(
            'date_click',
            [
                'header' => __('Date'),
                'align' => 'left',
                'index' => 'date_click',
                'type' => 'date',
            ]
        );
        return parent::_prepareColumns();
    }
}
