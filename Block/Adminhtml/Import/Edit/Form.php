<?php

namespace Magenest\Slider\Block\Adminhtml\Import\Edit;

use \Magento\Backend\Block\Widget\Form\Generic;

/**
 * Class Form
 *
 * @package Magenest\Slider\Block\Adminhtml\Import\Edit
 */
class Form extends Generic
{

    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * Form constructor.
     *
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry             $registry
     * @param \Magento\Framework\Data\FormFactory     $formFactory
     * @param \Magento\Store\Model\System\Store       $systemStore
     * @param array                                   $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        array $data = array()
    ) {
        $this->_systemStore = $systemStore;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Init form
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        $form = $this->_formFactory->create(
            array(
                'data' => array(
                    'id' => 'edit_form',
                    'action' => $this->getUrl('slider/import/save'),//@TODO: Them action
                    'method' => 'post',
                    'enctype' => 'multipart/form-data',
                ),
            )
        );

        $fieldsets['upload'] = $form->addFieldset('upload_file_fieldset', array('legend' => __('Import Slider')));
        $fieldsets['upload']->addField(
            \Magento\ImportExport\Model\Import::FIELD_NAME_SOURCE_FILE,
            'file',
            array(
                'name' => \Magento\ImportExport\Model\Import::FIELD_NAME_SOURCE_FILE,
                'label' => __('Import csv or xml file'),
                'title' => __('Select File to Import'),
                'required' => true,
                'class' => 'input-file',
            )
        );
        $fieldsets['upload']->addField(
            'note',
            'note',
            array(
                'after_element_html' => $this->getDownloadSampleFileHtml(),
            )
        );
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Get download sample file html
     *
     * @return string
     */
    protected function getDownloadSampleFileHtml()
    {
        $linkDownloadSample = $this->getDownloadSampleFile();

        $html = "<div id='sample-file-span' class='no-display' style='display: inline;margin-left: calc( (100%) * .5);'><a id='sample-file-link' href='{$linkDownloadSample}'>"
            . __('Download Sample File')
            . "</a></div>";
        return $html;
    }

    public function getDownloadSampleFile()
    {
        return $this->getUrl('slider/download/index');
    }
}
