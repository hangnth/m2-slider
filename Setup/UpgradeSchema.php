<?php

namespace Magenest\Slider\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade( SchemaSetupInterface $setup, ModuleContextInterface $context )
    {
        $setup->startSetup();

        if(version_compare($context->getVersion(), '1.0.1') < 0) {
                $table = $setup->getConnection()->newTable(
                    $setup->getTable('magenest_slider_report')
                )->addColumn(
                    'report_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    10,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                    'Report ID'
                )->addColumn(
                    'slider_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    10,
                    ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                    'Slider ID'
                )->addColumn(
                    'clicks',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    10,
                    ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                    'Banner/Slider Clicks'
                )->addColumn(
                    'date_click',
                    \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                    null,
                    ['nullable' => true],
                    'Banner/SLider click date time'
                )->addForeignKey(
                    $setup->getConnection()->getForeignKeyName(
                        $setup->getTable('magenest_slider_entity'),
                        'slider_id',
                        'magenest_slider_report',
                        'slider_id'
                    ),
                    'slider_id',
                    $setup->getTable('magenest_slider_entity'),
                    'slider_id',
                    Table::ACTION_CASCADE
                );
                $setup->getConnection()->createTable($table);
        }

        // Remove column position
        if (version_compare($context->getVersion(), '1.0.2') < 0 ) {
            $setup->getConnection()->dropColumn($setup->getTable('magenest_slider_entity'), 'position');
        }

        // Add column validate
        if (version_compare($context->getVersion(), '1.0.3') < 0 ) {
            $setup->getConnection()->addColumn(
                $setup->getTable('magenest_slider_entity'),
                'active_from',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                    'length' => null,
                    'nullable' => true,
                    'comment' => 'Active From Date'
                ]
            );
            $setup->getConnection()->addColumn(
                $setup->getTable('magenest_slider_entity'),
                'active_to',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                    'length' => null,
                    'nullable' => true,
                    'comment' => 'Active To Date'
                ]
            );
        }

        // Remove column active_from
        if (version_compare($context->getVersion(), '1.0.4') < 0 ) {
            $setup->getConnection()->dropColumn($setup->getTable('magenest_slider_entity'), 'active_from');
        }

        // Remove column active_to
        if (version_compare($context->getVersion(), '1.0.4') < 0 ) {
            $setup->getConnection()->dropColumn($setup->getTable('magenest_slider_entity'), 'active_to');
        }

        // Add column name
        if (version_compare($context->getVersion(), '1.0.5') < 0 ) {
            $setup->getConnection()->addColumn(
                $setup->getTable('magenest_slider_report'),
                'name',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'nullable' => false,
                    'comment' => 'Slider Name'
                ]
            );
        }

        $setup->endSetup();
    }
}
