<?php
/**
 * Copyright © 2020 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Magenest_Slider extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package Magenest_Slider
 */

namespace Magenest\Slider\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magenest\Slider\Controller\Adminhtml\Import\Save;

class UpgradeData extends Save implements UpgradeDataInterface
{
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '1.1.0', '<')) {
            $rootPath = $this->_path->getPath('module', 'Magenest_Slider');
            $rootPub = $this->_dir->getPath('media');
            if (!file_exists($rootPub . '/catalog/tmp/category')) {
                mkdir($rootPub . '/catalog/tmp/category', 0777, true);
            }
            $filePath = $rootPath . '/view/adminhtml/web/images';
            $copyFileFullPath = $rootPub . '/catalog/tmp/category';

            $this->copy_directory($filePath, $copyFileFullPath);
            //upload file
            $uploadedFile = $rootPath . '/Files/Sample/SampleFile.csv';

            $csvRawData = $this->csvProcessor->getData($uploadedFile);
            unset($csvRawData[0]);
            foreach ($csvRawData as $data) {
                $sampleData[self::NAME] = $data[0];
                $sampleData[self::STATUS] = $data[1];
                $sampleData[self::TYPE] = $data[2];
                $sampleData[self::DATA_SOURCE_SLIDER] = $data[3];
                $sampleData[self::PARENT_ID] = $data[4];
                $sampleData[self::DATA_SOURCE_ITEM] = $data[5];
                $sampleData[self::ORDER_NUMBER] = $data[6];
                if (count($this->getSliderCollection()->addFieldToFilter('name', ['eq' => $sampleData[self::NAME]]))) {
                    $sampleData[self::NAME] = $sampleData[self::NAME] . '-1';
                }
                $this->saveSlider($sampleData);
            }
        }

        $this->getSliderCollection()->save();
        $this->getItemCollection()->save();

    }

    public function getSliderCollection()
    {
        return $this->_slider->getCollection();
    }

    public function getItemCollection()
    {
        return $this->_item->getCollection();
    }
}
