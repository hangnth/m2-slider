define(
    [
        "jquery", "jquery/ui"
    ],
    function ($) {
        return function (datepicker) {
            $(document).ready(
                function () {
                    var field = datepicker.id;
                    $(field).calendar(
                        {
                            dateFormat: "yy-mm-dd",
                            showButtonPanel: true,
                            changeMonth: true,
                            changeYear: true,
                            yearRange: "c-10:c+05",
                            showsTime: false,
                            hideIfNoPrevNext: true,
                            minDate: new Date(),
                        }
                    );
                    $(".ui-datepicker-trigger").removeAttr("style");
                    $(".control-value").hide();
                    $(".ui-datepicker-trigger").click(
                        function () {
                            $(this).prev().focus();
                        }
                    );
                }
            );
        }
    }
);
